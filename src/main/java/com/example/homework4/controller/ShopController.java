package com.example.homework4.controller;

import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import com.example.homework4.service.ShopService;
import com.example.homework4.utils.dto.CartDtoGet;
import com.example.homework4.utils.dto.ProductDtoGet;
import com.example.homework4.utils.dto.ShopDtoGet;
import com.example.homework4.utils.dto.ShopDtoSave;
import com.example.homework4.utils.mapper.CartMapper;
import com.example.homework4.utils.mapper.ProductMapper;
import com.example.homework4.utils.mapper.ShopMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/shop")
public class ShopController {

    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @PostMapping("/add")
    public ResponseEntity<ShopDtoGet> addShop(@RequestBody ShopDtoSave shop) {

        return ResponseEntity.ok(ShopMapper.toDtoGet(shopService.createShop(ShopMapper.fromDtoSave(shop))));
    }

    @GetMapping("/id{id}")
    public ResponseEntity<ShopDtoGet> getShop(@PathVariable String id) {

        return ResponseEntity.ok(ShopMapper.toDtoGet(shopService.findShopById(Long.parseLong(id))));
    }

    @GetMapping("/all")
    public ResponseEntity<List<ShopDtoGet>> getAllShops() {

        return ResponseEntity.ok(shopService.getAllShops().stream()
                .map(ShopMapper::toDtoGet)
                .collect(Collectors.toList()));
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductDtoGet>> getAllShopProducts(@RequestBody Shop shop) {

        return ResponseEntity.ok(shopService.getAllProducts(shop).stream()
                .map(ProductMapper::toDtoGet)
                .collect(Collectors.toList()));
    }

    @PutMapping("/update/id{id}")
    public ResponseEntity<ShopDtoGet> updateShop(@PathVariable String id, @RequestBody ShopDtoSave shop) {

        return ResponseEntity.ok(ShopMapper.toDtoGet(shopService.updateShop(Long.parseLong(id),
                ShopMapper.fromDtoSave(shop))));
    }

    @DeleteMapping("/delete")
    public void deleteShop(@RequestBody Shop shop) {
        shopService.deleteShopById(shop);
    }


    @PutMapping("/{shop}/add-product/person/{id}")
    public ResponseEntity<CartDtoGet> addToCart(@PathVariable String shop, @RequestBody Product product,
                                                @PathVariable String id) {
        return ResponseEntity.ok(CartMapper.toDtoGet(
                shopService.addProductToCart(Long.parseLong(id), product, Long.parseLong(shop))));
    }

    @DeleteMapping("/{shop}/delete-product/person/{id}")
    public void deleteProduct(@PathVariable String shop, @RequestBody Product product, @PathVariable String id) {
        shopService.deleteProductFromCart(Long.parseLong(id), product, Long.parseLong(shop));
    }

    @GetMapping("/{shop}/get-product/person/{id}")
    public ResponseEntity<ProductDtoGet> getProduct(@PathVariable String shop,
                                                    @RequestBody Product product, @PathVariable String id) {
        return ResponseEntity.ok(ProductMapper.toDtoGet(
                shopService.getProductFromCart(Long.parseLong(id), product, Long.parseLong(shop))));
    }

}
