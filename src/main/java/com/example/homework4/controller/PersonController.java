package com.example.homework4.controller;

import com.example.homework4.model.Person;
import com.example.homework4.service.PersonService;
import com.example.homework4.utils.dto.PersonDtoGet;
import com.example.homework4.utils.dto.PersonDtoSave;
import com.example.homework4.utils.mapper.PersonMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/add")
    public ResponseEntity<PersonDtoGet> addPerson(@RequestBody PersonDtoSave person) {

        return ResponseEntity.ok(PersonMapper.toDtoGet(personService.save(PersonMapper.fromDtoSave(person))));
    }

    @GetMapping("/id{id}")
    public ResponseEntity<PersonDtoGet> getPerson(@PathVariable String id) {

        return ResponseEntity.ok(PersonMapper.toDtoGet(personService.findPersonById(Long.parseLong(id))));
    }

    @GetMapping("/all")
    public ResponseEntity<List<PersonDtoGet>> allPersons() {

        return ResponseEntity.ok(personService.getAllPersons().stream()
                .map(PersonMapper::toDtoGet)
                .collect(Collectors.toList()));
    }

    @PutMapping("/update/id{id}")
    public ResponseEntity<PersonDtoGet> updatePerson(@PathVariable String id, @RequestBody PersonDtoSave person) {

        return ResponseEntity.ok(PersonMapper.toDtoGet(
                personService.updatePerson(Long.parseLong(id), PersonMapper.fromDtoSave(person))));
    }

    @DeleteMapping("/delete")
    public void deletePerson(@RequestBody Person person) {
        personService.deletePersonById(person);
    }

}
