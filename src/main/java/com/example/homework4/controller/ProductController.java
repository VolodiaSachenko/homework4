package com.example.homework4.controller;

import com.example.homework4.service.ProductService;
import com.example.homework4.utils.dto.ProductDtoGet;
import com.example.homework4.utils.dto.ProductDtoSave;
import com.example.homework4.utils.dto.ProductDtoUpdate;
import com.example.homework4.utils.mapper.ProductMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/add")
    public ResponseEntity<ProductDtoGet> addProduct(@RequestBody ProductDtoSave product) {

        return ResponseEntity.ok(ProductMapper.toDtoGet(productService.save(product)));
    }

    @GetMapping("/id{id}")
    public ResponseEntity<ProductDtoGet> getProduct(@PathVariable String id) {

        return ResponseEntity.ok(ProductMapper.toDtoGet(productService.findProductById(Long.parseLong(id))));
    }

    @GetMapping("/all")
    public ResponseEntity<List<ProductDtoGet>> getAllProducts() {

        return ResponseEntity.ok(productService.getAllProducts().stream()
                .map(ProductMapper::toDtoGet)
                .collect(Collectors.toList()));
    }

    @PutMapping("/update/id{id}")
    public ResponseEntity<ProductDtoGet> updateProduct(@PathVariable String id,
                                                       @RequestBody ProductDtoUpdate product) {
        return ResponseEntity.ok(ProductMapper.toDtoGet(productService.updateProduct(Long.parseLong(id),
                ProductMapper.fromDtoUpdate(product))));
    }

    @DeleteMapping("/delete/{id}")
    public void deleteProduct(@PathVariable String id) {

        productService.deleteProductById(Long.parseLong(id));
    }

}
