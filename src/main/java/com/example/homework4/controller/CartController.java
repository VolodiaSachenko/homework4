package com.example.homework4.controller;

import com.example.homework4.model.Cart;
import com.example.homework4.service.CartService;
import com.example.homework4.utils.dto.CartDtoGet;
import com.example.homework4.utils.dto.CartDtoSave;
import com.example.homework4.utils.mapper.CartMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cart")
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/add")
    public ResponseEntity<?> addCart(@RequestBody CartDtoSave cartDto) {

        return ResponseEntity.ok(CartMapper.toDtoGet(cartService.save(cartDto)));
    }

    @GetMapping("/id{id}")
    public ResponseEntity<?> getCart(@PathVariable String id) {

        return ResponseEntity.ok(CartMapper.toDtoGet(cartService.findById(Long.parseLong(id))));
    }

    @GetMapping("/all")
    public ResponseEntity<List<CartDtoGet>> getAllCarts() {

        return ResponseEntity.ok(cartService.getAll().stream()
                .map(CartMapper::toDtoGet)
                .collect(Collectors.toList()));
    }

    @PutMapping("/update/id{id}")
    public ResponseEntity<CartDtoGet> updateCart(@PathVariable String id, @RequestBody CartDtoSave cart) {

        return ResponseEntity.ok(CartMapper.toDtoGet(cartService.update(Long.parseLong(id), CartMapper.fromDtoSave(cart))));
    }

    @DeleteMapping("/delete")
    public void deleteCart(@RequestBody Cart cart) {
        cartService.deleteById(cart.getId());
    }

}
