package com.example.homework4.repository;

import com.example.homework4.model.Cart;
import com.example.homework4.model.Person;
import com.example.homework4.model.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {

    Cart findByPersonIdAndShopId(Person person, Shop shop);

}
