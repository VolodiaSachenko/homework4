package com.example.homework4.repository;

import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByShopId(Shop shop);

}
