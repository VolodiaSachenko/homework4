package com.example.homework4.repository;

import com.example.homework4.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

}
