package com.example.homework4.service;

import com.example.homework4.model.Product;
import com.example.homework4.utils.dto.ProductDtoSave;

import java.util.List;

public interface ProductService {

    Product save(ProductDtoSave product);

    Product findProductById(Long id);

    List<Product> getAllProducts();

    Product updateProduct(Long id, Product product);

    void deleteProductById(Long id);

}
