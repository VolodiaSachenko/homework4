package com.example.homework4.service;

import com.example.homework4.model.Person;

import java.util.List;

public interface PersonService {

    Person save(Person person);

    Person findPersonById(Long id);

    List<Person> getAllPersons();

    Person updatePerson(Long id, Person person);

    void deletePersonById(Person person);

}
