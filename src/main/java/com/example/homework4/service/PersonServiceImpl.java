package com.example.homework4.service;

import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.exception.ValidateException;
import com.example.homework4.model.Person;
import com.example.homework4.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person save(Person person) {
        requestCheck(person);
        validateEmail(person.getEmail());
        validatePhone(person.getPhone());
        return personRepository.save(person);
    }

    @Override
    public Person findPersonById(Long id) {
        return personRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The person with id " + id + " doesn't exist."));
    }

    @Override
    public List<Person> getAllPersons() {
        return (List<Person>) personRepository.findAll();
    }

    @Override
    public Person updatePerson(Long id, Person person) {
        requestCheck(person);
        validateEmail(person.getEmail());
        validatePhone(person.getPhone());
        Person update = personRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The person with id " + id + " doesn't exist."));
        update.setFirstName(person.getFirstName());
        update.setSurName(person.getSurName());
        update.setPhone(person.getPhone());
        update.setEmail(person.getEmail());

        return personRepository.save(update);
    }

    @Override
    public void deletePersonById(Person person) {
        if (person.getId() == null) {
            throw new InvalidRequestException("personId is required.");
        }
        personRepository.deleteById(person.getId());
    }

    private static void requestCheck(Person person) {
        if (person.getFirstName() == null || person.getSurName() == null || person.getEmail() == null) {
            throw new InvalidRequestException("Fields firstName, surName and email are required.");
        }
    }

    private void validateEmail(String email) {
        if (personRepository.existsByEmail(email)) {
            throw new ValidateException("Email " + email + " already exists.");
        }
    }

    private void validatePhone(String phone) {
        if (personRepository.existsByPhone(phone)) {
            throw new ValidateException("Phone " + phone + " already exists.");
        }
    }

}
