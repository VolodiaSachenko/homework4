package com.example.homework4.service;

import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Cart;
import com.example.homework4.model.Person;
import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import com.example.homework4.repository.CartRepository;
import com.example.homework4.repository.PersonRepository;
import com.example.homework4.repository.ProductRepository;
import com.example.homework4.repository.ShopRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;
    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final PersonRepository personRepository;

    public ShopServiceImpl(ShopRepository shopRepository, ProductRepository productRepository,
                           CartRepository cartRepository, PersonRepository personRepository) {
        this.shopRepository = shopRepository;
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Shop createShop(Shop shop) {
        checkRequest(shop);
        if (shop.getProducts() == null) {
            shop.setProducts(new ArrayList<>());
        }
        return shopRepository.save(shop);
    }

    @Override
    public Shop findShopById(Long id) {
        return shopRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Invalid shop id: " + id));
    }

    @Override
    public List<Shop> getAllShops() {
        return (List<Shop>) shopRepository.findAll();
    }

    @Override
    public List<Product> getAllProducts(Shop shop) {
        if (shop.getId() == null) {
            throw new InvalidRequestException("Shop id is required.");
        }
        return productRepository.findByShopId(shop);
    }

    @Override
    public Shop updateShop(Long id, Shop shop) {
        Shop update = shopRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The shop with id " + id + " doesn't exist."));
        checkRequest(shop);
        update.setName(shop.getName());
        update.setDescription(shop.getDescription());
        update.setPhone(shop.getPhone());
        update.setRating(shop.getRating());

        return shopRepository.save(update);
    }

    @Override
    public void deleteShopById(Shop shop) {
        if (shop.getId() == null) {
            throw new InvalidRequestException("Shop id is required");
        }
        shopRepository.deleteById(shop.getId());
    }

    @Override
    public Cart addProductToCart(Long personId, Product product, Long shopId) {

        Person persistPerson = personRepository.findById(personId)
                .orElseThrow(() -> new NotFoundException("The person with id " + personId + " doesn't exist."));
        if (persistPerson.getCarts() == null) {
            persistPerson.setCarts(new ArrayList<>());
        }
        Shop shop = shopRepository.findById(shopId)
                .orElseThrow(() -> new NotFoundException("The shop with id " + shopId + " doesn't exist."));

        Product shopProduct = productRepository.findById(product.getId())
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + product.getId() + " doesn't exist."));
        checkProductKey(shopProduct.getShopId().getId(), shop.getId());
        checkCarts(persistPerson, shop, shopId);
        addToPersonCart(persistPerson, shopProduct, shop);

        personRepository.save(persistPerson);

        return cartRepository.findByPersonIdAndShopId(persistPerson, shop);
    }

    @Override
    public void deleteProductFromCart(Long personId, Product product, Long shopId) {
        Person persistPerson = personRepository.findById(personId)
                .orElseThrow(() -> new NotFoundException("The person with id " + personId + " doesn't exist."));
        Product personProduct = productRepository.findById(product.getId())
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + product.getId() + " doesn't exist."));
        checkProductKey(personProduct.getShopId().getId(), shopId);

        persistPerson.getCarts()
                .stream()
                .filter(cart -> Objects.equals(cart.getShopId().getId(), shopId))
                .findFirst()
                .ifPresent(cart -> {
                    if (cart.getProducts().size() != 0) {
                        if (cart.getProducts().remove(personProduct)) {
                            cart.setSum(cart.getSum().subtract(personProduct.getPrice()));
                        } else {
                            throw new InvalidRequestException("This product is not added to the cart.");
                        }
                    } else {
                        throw new InvalidRequestException("Cart is empty");
                    }
                });

        personRepository.save(persistPerson);
    }

    @Override
    public Product getProductFromCart(Long personId, Product product, Long shopId) {
        Person owner = personRepository.findById(personId)
                .orElseThrow(() -> new NotFoundException("The person with id " + personId + " doesn't exist."));

        Product personProduct = productRepository.findById(product.getId())
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + product.getId() + " doesn't exist."));

        return owner.getCarts()
                .stream()
                .filter(cart -> Objects.equals(shopId, cart.getShopId().getId()))
                .findFirst()
                .flatMap(cart -> cart.getProducts()
                        .stream()
                        .filter(findProduct -> Objects.equals(findProduct, personProduct))
                        .findFirst())
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + product.getId() + " doesn't exist."));
    }

    private Cart createCart(Person person, Shop shop) {
        Cart cart = new Cart();
        cart.setProducts(new ArrayList<>());
        cart.setShopId(shop);
        cart.setPersonId(person);
        cart.setOrderDate(new Date());
        cart.setSum(BigDecimal.ZERO);
        cart.setName(shop.getName() + " cart");

        return cartRepository.save(cart);
    }

    private static void checkRequest(Shop shop) {
        if (shop.getName() == null || shop.getDescription() == null || shop.getPhone() == null) {
            throw new InvalidRequestException("Fields name, description and phone are required.");
        }
    }

    private void checkCarts(Person persistPerson, Shop shop, Long shopId) {
        boolean isCartPresent = persistPerson.getCarts().stream()
                .anyMatch(cart -> Objects.equals(cart.getShopId().getId(), shopId));
        if (!isCartPresent) {
            persistPerson.getCarts().add(createCart(persistPerson, shop));
        }
    }

    private static void addToPersonCart(Person persistPerson, Product shopProduct, Shop shop) {
        persistPerson.getCarts().stream()
                .filter(cart -> Objects.equals(cart.getShopId().getId(), shop.getId()))
                .findFirst()
                .ifPresent(cart -> {
                    cart.getProducts().add(shopProduct);
                    cart.setSum(cart.getSum().add(shopProduct.getPrice()));
                });
    }

    private static void checkProductKey(Long productShopId, Long shopId) {
        if (productShopId.longValue() != shopId.longValue()) {
            throw new InvalidRequestException("This product belongs to other shop.");
        }
    }

}
