package com.example.homework4.service;

import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Cart;
import com.example.homework4.model.Person;
import com.example.homework4.repository.CartRepository;
import com.example.homework4.repository.PersonRepository;
import com.example.homework4.repository.ShopRepository;
import com.example.homework4.utils.dto.CartDtoSave;
import com.example.homework4.utils.mapper.CartMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;
    private final ShopRepository shopRepository;
    private final PersonRepository personRepository;

    public CartServiceImpl(CartRepository cartRepository, ShopRepository shopRepository,
                           PersonRepository personRepository) {
        this.cartRepository = cartRepository;
        this.shopRepository = shopRepository;
        this.personRepository = personRepository;
    }

    @Override
    public Cart save(CartDtoSave cartDtoSave) {
        Cart cart = CartMapper.fromDtoSave(cartDtoSave);
        if (cart.getName() == null) {
            throw new InvalidRequestException("Name is required.");
        }
        Person person = personRepository.findById(cartDtoSave.getPersonId())
                .orElseThrow(() -> new InvalidRequestException("Invalid person id: " + cartDtoSave.getPersonId()));
        if (person.getCarts().stream()
                .noneMatch(personCart -> Objects.equals(personCart.getShopId().getId(), cartDtoSave.getShopId()))) {

            cart.setPersonId(person);
            cart.setShopId(shopRepository.findById(cartDtoSave.getShopId())
                    .orElseThrow(() -> new InvalidRequestException("Invalid shop id: " + cartDtoSave.getPersonId())));
            cart.setOrderDate(new Date());
            cart.setProducts(new ArrayList<>());
            cart.setSum(BigDecimal.ZERO);
        } else {
            throw new InvalidRequestException("Cart already exists");
        }
        return cartRepository.save(cart);
    }

    @Override
    public Cart findById(Long id) {
        return cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The cart with id " + id + " doesn't exist."));
    }

    @Override
    public List<Cart> getAll() {

        return (List<Cart>) cartRepository.findAll();
    }

    @Override
    public Cart update(Long id, Cart cart) {
        Cart update = cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The cart with id " + id + " doesn't exist."));

        if (update.setName(cart.getName()) == null) {
            throw new InvalidRequestException("Name is required.");
        }

        return cartRepository.save(update);
    }

    @Override
    public void deleteById(Long id) {
        if (id == null) {
            throw new InvalidRequestException("Id is required");
        }
        cartRepository.deleteById(id);
    }

}
