package com.example.homework4.service;

import com.example.homework4.model.Cart;
import com.example.homework4.utils.dto.CartDtoSave;

import java.util.List;

public interface CartService {

    Cart save(CartDtoSave cart);

    Cart findById(Long id);

    List<Cart> getAll();

    Cart update(Long id, Cart cart);

    void deleteById(Long id);

}
