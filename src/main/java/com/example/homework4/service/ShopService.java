package com.example.homework4.service;

import com.example.homework4.model.Cart;
import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;

import java.util.List;

public interface ShopService {

    Shop createShop(Shop shop);

    Shop findShopById(Long id);

    List<Shop> getAllShops();

    List<Product> getAllProducts(Shop shop);

    Shop updateShop(Long id, Shop shop);

    void deleteShopById(Shop shop);

    Cart addProductToCart(Long personId, Product product, Long shopId);

    void deleteProductFromCart(Long personId, Product product, Long shopId);

    Product getProductFromCart(Long personId, Product product, Long shopId);

}
