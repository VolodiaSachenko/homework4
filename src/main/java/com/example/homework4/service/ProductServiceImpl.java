package com.example.homework4.service;

import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Product;
import com.example.homework4.repository.ProductRepository;
import com.example.homework4.repository.ShopRepository;
import com.example.homework4.utils.dto.ProductDtoSave;
import com.example.homework4.utils.mapper.ProductMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ShopRepository shopRepository;

    public ProductServiceImpl(ProductRepository productRepository, ShopRepository shopRepository) {
        this.productRepository = productRepository;
        this.shopRepository = shopRepository;
    }

    @Override
    public Product save(ProductDtoSave productDtoSave) {
        Product product = ProductMapper.fromDtoSave(productDtoSave);
        checkRequest(product);
        product.setShopId(shopRepository.findById(productDtoSave.getShopId())
                .orElseThrow(() -> new NotFoundException("The shop with id " + productDtoSave.getShopId() +
                        " doesn't exist.")));

        return productRepository.save(product);
    }

    @Override
    public Product findProductById(Long id) {

        return productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The product with id " + id + " doesn't exist."));
    }

    @Override
    public List<Product> getAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product updateProduct(Long id, Product product) {
        Product update = productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Invalid product id " + id));
        checkRequest(product);
        update.setName(product.getName());
        update.setDescription(product.getDescription());
        update.setPrice(product.getPrice());

        return productRepository.save(update);
    }

    @Override
    @Transactional
    public void deleteProductById(Long productId) {
        if (productId == null) {
            throw new InvalidRequestException("Invalid product id: " + productId);
        }
        productRepository.deleteById(productId);
    }

    private static void checkRequest(Product product) {
        if (product.getName() == null || product.getPrice() == null) {
            throw new InvalidRequestException("Fields name, shopId and price are required.");
        }
    }

}
