package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductDtoUpdate {

    private String name;
    private String description;
    private BigDecimal price;

}
