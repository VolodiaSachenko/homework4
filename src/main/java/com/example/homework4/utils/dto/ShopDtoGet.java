package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ShopDtoGet {

    private String name;
    private String description;
    private String phone;
    private float rating;
    private List<ProductDtoGet> products;

}
