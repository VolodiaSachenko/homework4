package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class CartDtoGet {

    private Long personId;
    private Long shopId;
    private String name;
    private BigDecimal sum;
    private List<ProductDtoGet> products;

}
