package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShopDtoSave {

    private String name;
    private String description;
    private String phone;
    private float rating;

}
