package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartDtoSave {

    private Long personId;
    private Long shopId;
    private String name;

}
