package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDtoSave {

    private String firstName;
    private String surName;
    private String phone;
    private String email;

}
