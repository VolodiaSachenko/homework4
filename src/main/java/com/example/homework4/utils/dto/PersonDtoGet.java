package com.example.homework4.utils.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PersonDtoGet {

    private String firstName;
    private String surName;
    private String phone;
    private String email;
    private List<CartDtoGet> carts;

}
