package com.example.homework4.utils.mapper;

import com.example.homework4.model.Product;
import com.example.homework4.utils.dto.ProductDtoGet;
import com.example.homework4.utils.dto.ProductDtoSave;
import com.example.homework4.utils.dto.ProductDtoUpdate;

import java.util.Date;

public class ProductMapper {

    public static Product fromDtoSave(ProductDtoSave productDtoSave) {

        return new Product()
                .setName(productDtoSave.getName())
                .setDescription(productDtoSave.getDescription())
                .setDateCreate(new Date())
                .setPrice(productDtoSave.getPrice());
    }

    public static ProductDtoGet toDtoGet(Product product) {
        ProductDtoGet productDtoGet = new ProductDtoGet();
        productDtoGet.setName(product.getName());
        productDtoGet.setDescription(product.getDescription());
        productDtoGet.setShopId(product.getShopId().getId());
        productDtoGet.setPrice(product.getPrice());

        return productDtoGet;
    }

    public static Product fromDtoUpdate(ProductDtoUpdate productDtoUpdate) {
        Product product = new Product();
        product.setName(productDtoUpdate.getName());
        product.setDescription(productDtoUpdate.getDescription());
        product.setPrice(productDtoUpdate.getPrice());

        return product;
    }

}
