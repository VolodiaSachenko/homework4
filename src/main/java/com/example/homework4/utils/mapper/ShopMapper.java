package com.example.homework4.utils.mapper;

import com.example.homework4.model.Shop;
import com.example.homework4.utils.dto.ShopDtoGet;
import com.example.homework4.utils.dto.ShopDtoSave;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ShopMapper {

    public static Shop fromDtoSave(ShopDtoSave shopDtoSave) {
        return new Shop()
                .setName(shopDtoSave.getName())
                .setDescription(shopDtoSave.getDescription())
                .setPhone(shopDtoSave.getPhone())
                .setRating(shopDtoSave.getRating());
    }

    public static ShopDtoGet toDtoGet(Shop shop) {
        ShopDtoGet shopDtoGet = new ShopDtoGet();
        shopDtoGet.setName(shop.getName());
        shopDtoGet.setDescription(shop.getDescription());
        shopDtoGet.setPhone(shop.getPhone());
        shopDtoGet.setRating(shop.getRating());
        shopDtoGet.setProducts(new ArrayList<>());
        if (shop.getProducts() != null) {
            shopDtoGet.setProducts(shop.getProducts()
                    .stream()
                    .map(ProductMapper::toDtoGet)

                    .collect(Collectors.toList()));
        }
        return shopDtoGet;
    }

}
