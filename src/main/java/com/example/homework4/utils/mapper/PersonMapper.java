package com.example.homework4.utils.mapper;

import com.example.homework4.model.Person;
import com.example.homework4.utils.dto.PersonDtoGet;
import com.example.homework4.utils.dto.PersonDtoSave;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

public class PersonMapper {

    public static Person fromDtoSave(PersonDtoSave personDtoSave) {
        return new Person()
                .setFirstName(personDtoSave.getFirstName())
                .setSurName(personDtoSave.getSurName())
                .setPhone(personDtoSave.getPhone())
                .setRegistrationDate(new Date())
                .setCarts(new ArrayList<>())
                .setEmail(personDtoSave.getEmail());
    }

    public static PersonDtoGet toDtoGet(Person person) {
        PersonDtoGet personDtoGet = new PersonDtoGet();
        personDtoGet.setFirstName(person.getFirstName());
        personDtoGet.setSurName(person.getSurName());
        personDtoGet.setEmail(person.getEmail());
        personDtoGet.setPhone(person.getPhone());
        personDtoGet.setCarts(new ArrayList<>());
        if (person.getCarts() != null) {
            personDtoGet.setCarts(person.getCarts()
                    .stream()
                    .map(CartMapper::toDtoGet)
                    .collect(Collectors.toList()));
        }
        return personDtoGet;
    }

}
