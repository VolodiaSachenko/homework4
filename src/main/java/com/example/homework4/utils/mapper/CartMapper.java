package com.example.homework4.utils.mapper;

import com.example.homework4.model.Cart;
import com.example.homework4.utils.dto.CartDtoGet;
import com.example.homework4.utils.dto.CartDtoSave;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CartMapper {

    public static Cart fromDtoSave(CartDtoSave cartDtoSave) {
        return new Cart()
                .setName(cartDtoSave.getName());
    }

    public static CartDtoGet toDtoGet(Cart cart) {
        CartDtoGet cartDtoGet = new CartDtoGet();
        cartDtoGet.setName(cart.getName());
        cartDtoGet.setPersonId(cart.getPersonId().getId());
        cartDtoGet.setShopId(cart.getShopId().getId());
        cartDtoGet.setSum(cart.getSum());
        cartDtoGet.setProducts(new ArrayList<>());
        if (cart.getProducts() != null) {
            cartDtoGet.setProducts(cart.getProducts()
                    .stream()
                    .map(ProductMapper::toDtoGet)
                    .collect(Collectors.toList()));
        }
        return cartDtoGet;
    }

}
