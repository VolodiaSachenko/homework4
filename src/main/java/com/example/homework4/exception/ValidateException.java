package com.example.homework4.exception;

public class ValidateException extends RuntimeException {

    public ValidateException(String message) {
        super(message);
    }

}
