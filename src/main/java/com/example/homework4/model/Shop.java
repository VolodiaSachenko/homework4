package com.example.homework4.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "shops")
@Accessors(chain = true)
public class Shop {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Long id;
    private String name;
    private String description;
    private String phone;
    private float rating;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "shop_id")
    private List<Product> products;

}
