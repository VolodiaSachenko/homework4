INSERT INTO shopping.market.persons (id, email, first_name, phone, registration, last_name) VALUES (1, 'petrenko@gmail.com', 'Anton', '+38066343324', '2022-09-22 16:52:57.530000', 'Petrenko');
INSERT INTO shopping.market.persons (id, email, first_name, phone, registration, last_name) VALUES (2, 'antonenko@gmail.com', 'Anton', '+38073343324', '2022-09-22 16:53:01.598000', 'Antonenko');
INSERT INTO shopping.market.persons (id, email, first_name, phone, registration, last_name) VALUES (3, 'igorenko@gmail.com', 'Igor', '+38086343324', '2022-09-22 16:53:03.942000', 'Igorenko');
INSERT INTO shopping.market.persons (id, email, first_name, phone, registration, last_name) VALUES (4, 'vasylciv@gmail.com', 'Vasyl', '+38066345624', '2022-09-22 16:53:05.927000', 'Vasylciv');
INSERT INTO shopping.market.persons (id, email, first_name, phone, registration, last_name) VALUES (5, 'tarasiuk@gmail.com', 'Taras', '+38095643324', '2022-09-22 16:53:08.036000', 'Tarasiuk');

INSERT INTO shopping.market.shops (id, description, name, phone, rating) VALUES (1, 'Find your perfect electric guitar at Guitar Shop. Shop from a huge range of electric guitars from top brands like Fender and Gibson.', 'Guitar Shop', '+38097550312', 5);
INSERT INTO shopping.market.shops (id, description, name, phone, rating) VALUES (2, 'CarShop is the original no-haggle, no-hassle used car superstore. Every CarShop Certified used car comes with a 6 month / 6000 mile warranty', 'Car Shop', '+38066550312', 5);
INSERT INTO shopping.market.shops (id, description, name, phone, rating) VALUES (3, 'Test shop', 'Test Shop', '+38066550312', 5);

INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (1, '2022-09-22 16:53:47.630000', 'The Gibson Les Paul is a rock ‘n’ roll icon. A trend-setting electric guitar ever since the ‘50s, the classic LP has been used by some of the world’s most pioneering players — including Jimmy Page, Peter Frampton, Slash & countless others!', 'Gibson Les Paul', 1230.00, 1);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (2, '2022-09-22 16:53:51.236000', 'The Gibson SG is a solid-body electric guitar model introduced by Gibson in 1961 as the Gibson Les Paul SG', 'Gibson SG', 1400.00, 1);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (3, '2022-09-22 16:53:53.948000', 'The Fender Stratocaster shaped the face of early rock & roll and still molds modern sound today.', 'Fender Stratocaster', 1100.00, 1);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (4, '2022-09-22 16:53:56.254000', 'The Fender Telecaster, colloquially known as the Tele , is an electric guitar produced by Fender.', 'Fender Telecaster', 1150.00, 1);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (5, '2022-09-22 16:53:58.887000', 'The RG421 is an RG series solid body electric guitar model introduced by Ibanez in 2013. It essentially replaced the similar RG321MH. It is made in Indonesia.', 'Ibanez rg421', 980.00, 1);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (6, '2022-09-22 16:54:00.568000', 'The Civic ran away with this comparison. It outclasses the Corolla in almost every way we can think of.', 'Honda Civic', 23000.00, 2);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (7, '2022-09-22 16:54:02.169000', 'The BMW X5 is a mid-sized luxury SUV produced by BMW. The X5 made its debut in 1999 as the E53 model.', 'BMW X5', 20000.00, 2);
INSERT INTO shopping.market.products (id, date, description, name, price, shop_id) VALUES (8, '2022-09-22 16:54:03.969000', 'It is an automobile sold internationally by the Japanese auto manufacturer Toyota since 1982, spanning multiple generations.', 'Toyota Camry', 25000.00, 2);

INSERT INTO shopping.market.carts (id, name, date, sum, person_id, shop_id) VALUES (1, 'Car shop cart', '2022-09-22 16:54:08.207000', 0.00, 1, 2);
INSERT INTO shopping.market.carts (id, name, date, sum, person_id, shop_id) VALUES (2, 'Guitar shop cart', '2022-09-22 16:54:10.857000', 0.00, 2, 1);
