package com.example.homework4.blanks;

public class PersonBlank {

    public static final Long ID = 1L;

    public static final String FIRST_NAME = "Test person";

    public static final String SURNAME = "Test surname";

    public static final String EMAIL = "test@gmail.com";

    public static final String PHONE = "+38077044345";

}
