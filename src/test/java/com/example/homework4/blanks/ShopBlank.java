package com.example.homework4.blanks;

public class ShopBlank {

    public static final Long ID = 1L;

    public static final String NAME = "Guitar Shop";

    public static final String DESCRIPTION = "Find your perfect electric guitar at Guitar Shop.";

    public static final String PHONE = "+38097550312";

    public static final float RATING = 5.0f;

}
