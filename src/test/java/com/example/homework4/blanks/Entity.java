package com.example.homework4.blanks;

import com.example.homework4.model.Cart;
import com.example.homework4.model.Person;
import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import com.example.homework4.utils.dto.CartDtoSave;
import com.example.homework4.utils.dto.ProductDtoSave;

import java.util.ArrayList;
import java.util.Date;

import static com.example.homework4.blanks.PersonBlank.EMAIL;
import static com.example.homework4.blanks.PersonBlank.FIRST_NAME;
import static com.example.homework4.blanks.PersonBlank.ID;
import static com.example.homework4.blanks.PersonBlank.PHONE;
import static com.example.homework4.blanks.PersonBlank.SURNAME;

public class Entity {

    public static final Person PERSON = new Person()
            .setId(ID)
            .setFirstName(FIRST_NAME)
            .setSurName(SURNAME)
            .setEmail(EMAIL)
            .setPhone(PHONE)
            .setRegistrationDate(new Date())
            .setCarts(new ArrayList<>());

    public static final Shop SHOP = new Shop()
            .setId(ShopBlank.ID)
            .setName(ShopBlank.NAME)
            .setDescription(ShopBlank.DESCRIPTION)
            .setPhone(ShopBlank.PHONE)
            .setRating(ShopBlank.RATING)
            .setProducts(new ArrayList<>());

    public static final Product PRODUCT = new Product()
            .setId(ProductBlank.ID)
            .setName(ProductBlank.NAME)
            .setDescription(ProductBlank.DESCRIPTION)
            .setShopId(SHOP)
            .setDateCreate(new Date())
            .setPrice(ProductBlank.PRICE);

    public static final ProductDtoSave PRODUCT_DTO_SAVE = new ProductDtoSave();

    public static final Cart CART = new Cart()
            .setId(CartBlank.ID)
            .setName(CartBlank.NAME)
            .setOrderDate(new Date())
            .setPersonId(PERSON)
            .setShopId(SHOP)
            .setSum(PRODUCT.getPrice())
            .setProducts(new ArrayList<>());

    public static final CartDtoSave CART_DTO_SAVE = new CartDtoSave();

}