package com.example.homework4.blanks;

import java.math.BigDecimal;

public class ProductBlank {

    public static final Long ID = 1L;

    public static final String NAME = "Gibson Les Paul";

    public static final String DESCRIPTION = "The Gibson Les Paul is a rock ‘n’ roll icon.";

    public static final BigDecimal PRICE = new BigDecimal(1230);

}
