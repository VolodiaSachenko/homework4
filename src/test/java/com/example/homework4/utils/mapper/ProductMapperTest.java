package com.example.homework4.utils.mapper;

import com.example.homework4.blanks.ProductBlank;
import com.example.homework4.blanks.ShopBlank;
import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import com.example.homework4.utils.dto.ProductDtoGet;
import com.example.homework4.utils.dto.ProductDtoSave;
import com.example.homework4.utils.dto.ProductDtoUpdate;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ProductMapperTest {

    @Test
    void fromDtoSave() {
        ProductDtoSave productDtoSave = new ProductDtoSave();
        productDtoSave.setName(ProductBlank.NAME);
        productDtoSave.setDescription(ProductBlank.DESCRIPTION);
        productDtoSave.setShopId(ShopBlank.ID);
        productDtoSave.setPrice(ProductBlank.PRICE);

        Shop shop = new Shop();
        shop.setId(ShopBlank.ID);

        Product product = ProductMapper.fromDtoSave(productDtoSave);
        product.setShopId(shop);

        assertEquals(product.getName(), productDtoSave.getName());
        assertEquals(product.getDescription(), productDtoSave.getDescription());
        assertEquals(product.getShopId().getId(), productDtoSave.getShopId());
        assertEquals(product.getPrice(), productDtoSave.getPrice());
    }

    @Test
    void toDtoGet() {
        Shop shop = new Shop();
        shop.setId(ShopBlank.ID);

        Product product = new Product();
        product.setName(ProductBlank.NAME);
        product.setDescription(ProductBlank.DESCRIPTION);
        product.setId(ProductBlank.ID);
        product.setPrice(ProductBlank.PRICE);
        product.setDateCreate(new Date());
        product.setShopId(shop);


        ProductDtoGet productDtoGet = ProductMapper.toDtoGet(product);

        assertEquals(product.getName(), productDtoGet.getName());
        assertEquals(product.getDescription(), productDtoGet.getDescription());
        assertEquals(product.getShopId().getId(), productDtoGet.getShopId());
        assertEquals(product.getPrice(), productDtoGet.getPrice());
    }

    @Test
    void fromDtoUpdate() {
        ProductDtoUpdate productDtoUpdate = new ProductDtoUpdate();
        productDtoUpdate.setName(ProductBlank.NAME);
        productDtoUpdate.setDescription(ProductBlank.DESCRIPTION);
        productDtoUpdate.setPrice(ProductBlank.PRICE);

        Product product = ProductMapper.fromDtoUpdate(productDtoUpdate);

        assertEquals(productDtoUpdate.getName(), product.getName());
        assertEquals(productDtoUpdate.getDescription(), product.getDescription());
        assertEquals(productDtoUpdate.getPrice(), product.getPrice());
    }
}