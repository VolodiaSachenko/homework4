package com.example.homework4.utils.mapper;

import com.example.homework4.blanks.CartBlank;
import com.example.homework4.blanks.PersonBlank;
import com.example.homework4.blanks.ShopBlank;
import com.example.homework4.model.Cart;
import com.example.homework4.model.Person;
import com.example.homework4.model.Shop;
import com.example.homework4.utils.dto.CartDtoGet;
import com.example.homework4.utils.dto.CartDtoSave;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CartMapperTest {

    @Test
    void fromDtoSave() {
        CartDtoSave cartDtoSave = new CartDtoSave();
        cartDtoSave.setName(CartBlank.NAME);

        Cart cart = CartMapper.fromDtoSave(cartDtoSave);

        assertEquals(cart.getName(), cartDtoSave.getName());
    }

    @Test
    void toDtoGet() {
        Cart cart = new Cart();

        Person person = new Person();
        person.setId(PersonBlank.ID);

        Shop shop = new Shop();
        shop.setId(ShopBlank.ID);

        cart.setSum(new BigDecimal(0));
        cart.setName(CartBlank.NAME);
        cart.setShopId(shop);
        cart.setPersonId(person);

        CartDtoGet cartDtoGet = CartMapper.toDtoGet(cart);

        assertEquals(cart.getSum(), cartDtoGet.getSum());
        assertEquals(cart.getName(), cartDtoGet.getName());
    }

}