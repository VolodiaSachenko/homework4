package com.example.homework4.utils.mapper;

import com.example.homework4.blanks.ShopBlank;
import com.example.homework4.model.Shop;
import com.example.homework4.utils.dto.ShopDtoGet;
import com.example.homework4.utils.dto.ShopDtoSave;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShopMapperTest {

    @Test
    void fromDtoSave() {
        ShopDtoSave shopDtoSave = new ShopDtoSave();
        shopDtoSave.setName(ShopBlank.NAME);
        shopDtoSave.setDescription(ShopBlank.DESCRIPTION);
        shopDtoSave.setPhone(ShopBlank.PHONE);
        shopDtoSave.setRating(ShopBlank.RATING);

        Shop shop = ShopMapper.fromDtoSave(shopDtoSave);

        assertEquals(shop.getName(), shopDtoSave.getName());
        assertEquals(shop.getDescription(), shopDtoSave.getDescription());
        assertEquals(shop.getPhone(), shopDtoSave.getPhone());
        assertEquals(shop.getRating(), shopDtoSave.getRating());
    }

    @Test
    void toDtoGet() {
        Shop shop = new Shop();
        shop.setName(ShopBlank.NAME);
        shop.setDescription(ShopBlank.DESCRIPTION);
        shop.setPhone(ShopBlank.PHONE);
        shop.setRating(ShopBlank.RATING);

        ShopDtoGet shopDtoGet = ShopMapper.toDtoGet(shop);

        assertEquals(shop.getName(), shopDtoGet.getName());
        assertEquals(shop.getDescription(), shopDtoGet.getDescription());
        assertEquals(shop.getPhone(), shopDtoGet.getPhone());
        assertEquals(shop.getRating(), shopDtoGet.getRating());
    }
}