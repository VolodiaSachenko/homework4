package com.example.homework4.utils.mapper;

import com.example.homework4.blanks.PersonBlank;
import com.example.homework4.model.Person;
import com.example.homework4.utils.dto.PersonDtoGet;
import com.example.homework4.utils.dto.PersonDtoSave;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonMapperTest {

    @Test
    void fromDtoSave() {
        PersonDtoSave personDto = new PersonDtoSave();
        personDto.setFirstName(PersonBlank.FIRST_NAME);
        personDto.setSurName(PersonBlank.SURNAME);
        personDto.setPhone(PersonBlank.PHONE);
        personDto.setEmail(PersonBlank.EMAIL);

        Person person = PersonMapper.fromDtoSave(personDto);

        assertEquals(person.getSurName(), personDto.getSurName());
        assertEquals(person.getFirstName(), personDto.getFirstName());
        assertEquals(person.getPhone(), personDto.getPhone());
        assertEquals(person.getEmail(), personDto.getEmail());
    }

    @Test
    void toDtoGet() {
        Person person = new Person();
        person.setFirstName(PersonBlank.FIRST_NAME);
        person.setSurName(PersonBlank.SURNAME);
        person.setPhone(PersonBlank.PHONE);
        person.setEmail(PersonBlank.EMAIL);

        PersonDtoGet personDtoGet = PersonMapper.toDtoGet(person);

        assertEquals(person.getSurName(), personDtoGet.getSurName());
        assertEquals(person.getFirstName(), personDtoGet.getFirstName());
        assertEquals(person.getPhone(), personDtoGet.getPhone());
        assertEquals(person.getEmail(), personDtoGet.getEmail());
    }
}