package com.example.homework4.service;

import com.example.homework4.blanks.Entity;
import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Product;
import com.example.homework4.utils.dto.ProductDtoSave;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductServiceImplTest {

    private final Product product = Entity.PRODUCT;
    private final ProductDtoSave productDtoSave = Entity.PRODUCT_DTO_SAVE;
    @Autowired
    private ProductServiceImpl productService;

    @Test
    void save() {
        productDtoSave.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                productService.save(productDtoSave));
        assertEquals("Fields name, shopId and price are required.", requestException.getMessage());
    }

    @Test
    void get() {
        product.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                productService.findProductById(product.getId()));
        assertEquals("The product with id 100 doesn't exist.", requestException.getMessage());
    }

    @Test
    void update() {
        product.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                productService.updateProduct(product.getId(), product));
        assertEquals("Invalid product id 110", requestException.getMessage());
    }

}