package com.example.homework4.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.example.homework4.blanks.Entity;
import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.exception.ValidateException;
import com.example.homework4.model.Person;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PersonServiceImplTest {

    private final Person person = Entity.PERSON;
    @Autowired
    private PersonServiceImpl personService;

    @Test
    void save() {
        person.setEmail(null);
        Throwable request = assertThrows(InvalidRequestException.class, () -> personService.save(person));
        assertEquals("Fields firstName, surName and email are required.", request.getMessage());

        person.setEmail("petrenko@gmail.com");
        Throwable validate = assertThrows(ValidateException.class, () -> personService.save(person));
        assertEquals("Email petrenko@gmail.com already exists.", validate.getMessage());
    }

    @Test
    void get() {
        person.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.findPersonById(person.getId()));
        assertEquals("The person with id 100 doesn't exist.", requestException.getMessage());
    }

    @Test
    void update() {
        person.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.updatePerson(person.getId(), person));
        assertEquals("The person with id 110 doesn't exist.", requestException.getMessage());
    }

}