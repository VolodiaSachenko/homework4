package com.example.homework4.service;

import com.example.homework4.blanks.Entity;
import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Product;
import com.example.homework4.model.Shop;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ShopServiceImplTest {

    private final Shop shop = Entity.SHOP;
    private final Product product = Entity.PRODUCT;
    @Autowired
    private ShopServiceImpl shopService;

    @Test
    void save() {
        shop.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                shopService.createShop(shop));
        assertEquals("Fields name, description and phone are required.", requestException.getMessage());
    }

    @Test
    void get() {
        shop.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.findShopById(shop.getId()));
        assertEquals("Invalid shop id: 100", requestException.getMessage());
    }

    @Test
    void update() {
        shop.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.updateShop(shop.getId(), shop));
        assertEquals("The shop with id 110 doesn't exist.", requestException.getMessage());
    }

    @Test
    void addProductToCart() {
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.addProductToCart(100L, product, 1L));
        assertEquals("The person with id 100 doesn't exist.", requestException.getMessage());

        Throwable requestExceptionShop = assertThrows(NotFoundException.class, () ->
                shopService.addProductToCart(1L, product, 100L));
        assertEquals("The shop with id 100 doesn't exist.", requestExceptionShop.getMessage());

    }

    @Test
    void deleteProductFromCart() {
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.deleteProductFromCart(100L, product, 1L));
        assertEquals("The person with id 100 doesn't exist.", requestException.getMessage());

        product.setId(100L);
        Throwable requestExceptionProduct = assertThrows(NotFoundException.class, () ->
                shopService.deleteProductFromCart(1L, product, 1L));
        assertEquals("The product with id 100 doesn't exist.", requestExceptionProduct.getMessage());

    }

    @Test
    void getProductFromCart() {
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.getProductFromCart(100L, product, 1L));
        assertEquals("The person with id 100 doesn't exist.", requestException.getMessage());

        product.setId(100L);
        Throwable requestExceptionProduct = assertThrows(NotFoundException.class, () ->
                shopService.getProductFromCart(1L, product, 1L));
        assertEquals("The product with id 100 doesn't exist.", requestExceptionProduct.getMessage());
    }
}