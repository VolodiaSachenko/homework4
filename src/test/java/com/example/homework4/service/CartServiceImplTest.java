package com.example.homework4.service;

import com.example.homework4.blanks.Entity;
import com.example.homework4.exception.InvalidRequestException;
import com.example.homework4.exception.NotFoundException;
import com.example.homework4.model.Cart;
import com.example.homework4.utils.dto.CartDtoSave;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CartServiceImplTest {

    private final Cart cart = Entity.CART;
    private final CartDtoSave cartDtoSave = Entity.CART_DTO_SAVE;
    @Autowired
    private CartServiceImpl cartService;

    @Test
    void save() {
        cartDtoSave.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () -> cartService.save(cartDtoSave));
        assertEquals("Name is required.", requestException.getMessage());
    }

    @Test
    void get() {
        cart.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                cartService.findById(cart.getId()));
        assertEquals("The cart with id 100 doesn't exist.", requestException.getMessage());
    }

    @Test
    void update() {
        cart.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                cartService.update(cart.getId(), cart));
        assertEquals("The cart with id 110 doesn't exist.", requestException.getMessage());
    }

}